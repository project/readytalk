CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The readytalk module adds Webform integration with
ReadyTalk (http://www.readytalk.com). It adds registrants into ReadyTalk using
ReadyTalk API v1.3(https://cc.readytalk.com/api/1.3/rest).


REQUIREMENTS
------------
This module requires the following modules:
  * User (core)
  * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Configure user permissions in Administration » People » Permissions:

- Administer Readytalk settings

  This permission allows users to add or update readytalk settings.

MAINTAINERS
-----------
Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
